# JAXWS Maven Plugin

Git clone of jaxws-maven-plugin.  
Cloned on Juli 10, 2014 to fix an issue with Java 8.  
This repo is cloned from the original sources using command

```
#!shell

git svn clone svn+ssh://diversit@svn.java.net/jax-ws-commons~svn --stdlayout --prefix=svn/ --include-paths=jaxws-maven-plugin
```
This took all history from the original sources but only included the jaxws-maven-plugin sources not any of the other projects in the original subversion repo.

## Issue JAX_WS_COMMONS-129
The issue related to the problem with Java 8 is [https://java.net/jira/browse/JAX_WS_COMMONS-129]  
The fix for this issue is on branch JAX_WS_COMMONS-129.  

## References
Original site : [https://jax-ws-commons.java.net/]  
Original Jira : [https://java.net/jira/browse/JAX_WS_COMMONS/]  
Original source code (subversion) : [https://java.net/projects/jax-ws-commons/sources/svn/show]